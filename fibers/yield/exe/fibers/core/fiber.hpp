#pragma once

#include <exe/fibers/core/routine.hpp>
#include <exe/fibers/core/scheduler.hpp>

#include <exe/coro/core.hpp>
#include <twist/ed/local/ptr.hpp>

namespace exe::fibers {

// Fiber = stackful coroutine + scheduler (thread pool)

class Fiber {
 public:
  explicit Fiber(Routine routine, Scheduler* sched);

  void Schedule();

  static Fiber* Self();

  void SuspendCoroutine();

 private:
  // Task
  void Run();

 private:
  coro::CoroutineCore coro_;
  Scheduler* scheduler_;
};

}  // namespace exe::fibers
