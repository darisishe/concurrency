#pragma once

#include <exe/tp/task.hpp>

#include <exe/support/queue.hpp>
#include <exe/support/wait_group.hpp>

#include <twist/ed/stdlike/thread.hpp>

#include <vector>

namespace exe::tp {

// Fixed-size pool of worker threads

class ThreadPool {
 public:
  explicit ThreadPool(size_t threads);
  ~ThreadPool();

  // Non-copyable
  ThreadPool(const ThreadPool&) = delete;
  ThreadPool& operator=(const ThreadPool&) = delete;

  // Non-movable
  ThreadPool(ThreadPool&&) = delete;
  ThreadPool& operator=(ThreadPool&&) = delete;

  // Launches worker threads
  void Start();

  // Schedules task for execution in one of the worker threads
  void Submit(Task);

  // Locates current thread pool from worker thread
  static ThreadPool* Current();

  // Waits until outstanding work count reaches zero
  void WaitIdle();

  // Stops the worker threads as soon as possible
  void Stop();

 private:
  void Worker();

 private:
  // Worker threads, task queue, etc
  std::vector<twist::ed::stdlike::thread> workers_;
  size_t num_threads_;
  UnboundedBlockingQueue<Task> tasks_;
  WaitGroup tasks_wg_;  // counts tasks
};

}  // namespace exe::tp
