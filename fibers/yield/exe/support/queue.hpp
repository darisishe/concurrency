#pragma once

#include <deque>
#include <optional>

#include <twist/ed/stdlike/mutex.hpp>
#include <twist/ed/stdlike/condition_variable.hpp>

namespace exe::tp {

// Unbounded blocking multi-producers/multi-consumers (MPMC) queue

template <typename T>
class UnboundedBlockingQueue {
 public:
  bool Put(T value) {
    std::lock_guard guard(mutex_);
    if (closed_) {
      return false;
    }

    buffer_.push_back(std::move(value));
    closed_or_not_empty_.notify_one();
    return true;
  }

  std::optional<T> Take() {
    std::unique_lock lock(mutex_);

    while (!closed_ && buffer_.empty()) {
      closed_or_not_empty_.wait(lock);
    }

    if (closed_ && buffer_.empty()) {
      return std::nullopt;
    } else {
      T front(std::move(buffer_.front()));
      buffer_.pop_front();
      return front;
    }
  }

  void Close() {
    std::lock_guard guard(mutex_);
    closed_ = true;
    closed_or_not_empty_.notify_all();
  }

 private:
  // Buffer
  std::deque<T> buffer_;  // protected by mutex_
  bool closed_ = false;
  twist::ed::stdlike::mutex mutex_;
  twist::ed::stdlike::condition_variable closed_or_not_empty_;
};

}  // namespace exe::tp
